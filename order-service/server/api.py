import json
import re
import traceback
from asyncio import gather
from functools import partial

from aiohttp import ClientSession
from peewee import EXCLUDED
from playhouse.shortcuts import model_to_dict
from tornado.ioloop import IOLoop
from tornado.web import RequestHandler
from tornado_swirl import restapi, describe

from server.config import WAREHOUSE_QUEUE, PAYMENT_QUEUE
from server.db import Order, manager, OrderItem
from server.messages import MessagesWorker
from utils.services_request import get, post
from utils.swagger import class_spec_copy

describe(title='Order service API', description='Order service API of microshop project')


class APIHandler(RequestHandler):
    async def prepare(self):
        self.response = {'success': True}

    def write_error(self, status_code, **kwargs):
        self.response = {
            'success': False,
            'exception': {
                'name': kwargs['exc_info'][0].__name__,
                'args': list(kwargs['exc_info'][1].args),
                'traceback': traceback.format_tb(kwargs['exc_info'][2]),
            }
        }
        exc_name = self.response['exception']['name']
        if exc_name.endswith('DoesNotExist'):
            self.set_status(404, f"{self.response['exception']['name'].replace('DoesNotExist', '')} Not Found")
        elif exc_name in {'AssertionError', 'MissingArgumentError'}:
            self.set_status(400)
        elif exc_name in {'IntegrityError'}:
            self.set_status(409)

    def set_response(self, response: dict):
        self.response.update(response)

    def set_default_headers(self):
        self.set_header('Content-Type', 'application/json')

    def finish(self, chunk=None):
        self.write(json.dumps(self.response, indent=2, default=str, sort_keys=True))
        super().finish(chunk)


@restapi('/')
class MainHandler(RequestHandler):
    def get(self):
        self.redirect('/swagger/spec.html')


order_to_dict = partial(model_to_dict, backrefs=True, exclude=[OrderItem.id])


@restapi(r'/api/orders/?')
class OrdersHandler(APIHandler):
    async def get(self):
        """Get list of orders

        Load list of orders from database.

        Query Parameters:
            user_id (int) -- Optional.  Filter orders by user.

        Tags:
            orders
        """
        query = Order.select()
        user_id = self.get_argument('user_id', None)
        if user_id:
            query = query.where(Order.user_id == int(user_id))

        orders = map(
            order_to_dict,
            await manager.prefetch(query, OrderItem.select())
        )
        self.set_response({
            'orders': list(orders)
        })


class AbstractOrderHandler(APIHandler):
    load_items = False

    async def prepare(self):
        await super().prepare()
        await self.load_order(self.load_items)

    async def load_order(self, load_items=True):
        if 'id' in self.path_kwargs or hasattr(self, 'order'):
            order_id = self.path_kwargs.get('id') or self.order.id
            queries = [
                Order.select().where(Order.id == int(order_id)),
            ]
            if load_items:
                queries.append(OrderItem.select().where(OrderItem.order_id == int(order_id)))
            self.order = (await manager.prefetch(*queries))[0]
            assert self.order.user_id == int(self.get_argument('user_id', self.order.user_id)), "Wrong order's owner"
        else:
            self.order = await manager.create(Order, user_id=self.get_argument('user_id'))
        return self.order


@restapi(r'/api/orders/(?P<id>\d+)/?')
class OrderHandler(AbstractOrderHandler):
    load_items = True

    async def get(self, *args, **kwargs):
        """Get order

        Load order data from database.

        Path Parameters:
            id (int) -- id of order.

        Tags:
            orders
        """
        self.set_response(
            order_to_dict(self.order)
        )


@restapi(r'/api/orders/add/?')
@class_spec_copy
@restapi(r'/api/orders/(?P<id>\d+)/add/?')
class OrderAddItemHandler(AbstractOrderHandler):
    async def post(self, *args, **kwargs):
        """Add item to order

        Add item to order. If no order id is specified, new order gets created.

        Path Parameters:
            id (int) -- id of order.

        Query Parameters:
            user_id (int) -- Optional.  It is used when new order should be created.
            item_id (int) -- Required.  Id of item that should be added.
            amount (float) --  Quantity of items to be added.
                default: 1

        Tags:
            orders
        """
        item_id = int(self.get_argument('item_id'))
        amount = float(self.get_argument('amount', '1'))
        assert amount > 0, 'amount should be positive'

        assert (await get('warehouse', f'api/warehouse/items/{item_id}/'))['amount'] >= amount, 'Not enough items'
        await manager.execute(
            OrderItem.insert(order=self.order, item_id=item_id, amount=amount).on_conflict(
                conflict_target=('order_id', 'item_id'),
                update={OrderItem.amount: OrderItem.amount + EXCLUDED.amount}
            )
        )
        self.set_response(order_to_dict(
            await self.load_order()
        ))


del OrderAddItemHandler.post.path_spec.path_params['id']


@restapi(r'/api/orders/(?P<id>\d+)/pay/?')
class OrderPayHandler(AbstractOrderHandler):
    load_items = True

    async def post(self, *args, **kwargs):
        """Pay order

        Pay and proceed order

        Path Parameters:
            id (int) -- id of order.

        Tags:
            orders
        """
        async with ClientSession() as session:
            async def assert_enough(item: OrderItem):
                resp = await get('warehouse', f'api/warehouse/items/{item.item_id}/', session=session)
                assert resp['amount'] >= item.amount, 'Not enough items'

            await gather(*(
                assert_enough(item) for item in self.order.items
            ))
            await gather(*(
                MessagesWorker.send_message(WAREHOUSE_QUEUE, {
                    'item_id': item.item_id,
                    'amount': -item.amount
                }) for item in self.order.items
            ))
            try:
                await post(
                    'payment',
                    '/payment/create/',
                    raise_for_status=True,
                    json={
                        'payment': {
                            'orderID': self.order.id,
                        'status': 'Payed'
                        }
                    }
                )
                self.order.status = 'Payed'
                await manager.update(self.order, only=[Order.status])
            except Exception:
                # revert items changes
                await gather(*(
                    MessagesWorker.send_message(WAREHOUSE_QUEUE, {
                        'item_id': item.item_id,
                        'amount': item.amount
                    }) for item in self.order.items
                ))
                raise


@restapi(r'/api/orders/(?P<id>\d+)/status/?')
class OrderChangeStatusHandler(AbstractOrderHandler):
    load_items = True

    async def put(self, *args, **kwargs):
        """Change status of order

        Change status of order.

        Path Parameters:
            id (int) -- id of order.

        Query Parameters:
            status (str) -- Required.  New status.

        Tags:
            orders
        """
        status = self.get_argument('status')
        self.order.status = status
        IOLoop.current().add_callback(MessagesWorker.send_message, PAYMENT_QUEUE, {
            'order_id': self.order.id,
            'status': status
        })
        if re.match(r'cancell?ed', status.lower()):
            for item in self.order.items:
                IOLoop.current().add_callback(MessagesWorker.send_message, WAREHOUSE_QUEUE, {
                    'item_id': item.item_id,
                    'amount': item.amount
                })

        await manager.update(self.order, ['status'])
        self.set_response(order_to_dict(
            await self.load_order(False),
            backrefs=False
        ))
