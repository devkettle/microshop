from datetime import datetime

import peewee_async
import peewee_asyncext
from peewee import Model, IntegerField, AutoField, TextField, ForeignKeyField, DateTimeField, FloatField
from peeweedbevolve import evolve

from server.config import DB_HOST, DB_PASSWORD

db = peewee_asyncext.PooledPostgresqlExtDatabase(
    f'microshop',
    user='postgres',
    max_connections=8,
    host=DB_HOST,
    password=DB_PASSWORD
)

manager = peewee_async.Manager(db)


class BaseModel(Model):
    id = AutoField()

    class Meta:
        database = db


class Order(BaseModel):
    user_id = IntegerField(null=True)
    status = TextField(null=True)
    created = DateTimeField(default=datetime.now)


class OrderItem(BaseModel):
    order = ForeignKeyField(Order, backref='items')
    item_id = IntegerField()
    amount = FloatField()

    class Meta:
        indexes = (
            (('order', 'item_id'), True),
        )


evolve(db, interactive=False, ignore_tables=['basemodel'])
