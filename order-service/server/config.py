import asyncio
from os.path import join, abspath, exists

import uvloop
import yaml
from wasp_eureka import EurekaClient

from utils.other import get_instance_ip

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

PORT = 4001

MQ_URL = 'amqp://rabbit-mq/'

EUREKA_URL = 'http://eureka:8761'

DB_HOST = 'order-db'
DB_PASSWORD = 'postgres'

WAREHOUSE_QUEUE = 'queue'
PAYMENT_QUEUE = 'hello'

WORK_DIR = abspath(join(abspath(__file__), '../..'))
conf_filename = join(WORK_DIR, 'conf.yaml')

if exists(conf_filename):
    data = yaml.load(open(join(WORK_DIR, conf_filename)), Loader=yaml.BaseLoader)
    locals().update(data)

eureka = EurekaClient('order', PORT, eureka_url=EUREKA_URL, ip_addr=get_instance_ip(EUREKA_URL))
