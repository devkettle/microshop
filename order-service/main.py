import logging
import sys
from concurrent.futures import ThreadPoolExecutor

import uvloop
from tornado import httpserver
from tornado.ioloop import IOLoop

from server.app import OrderServiceApplication
from server.config import PORT
from server.messages import MessagesWorker
from utils.eureka import EurekaProcess


def main():
    log = logging.getLogger('')
    log.addHandler(logging.StreamHandler())
    log.setLevel(logging.DEBUG)

    MessagesWorker().start()
    EurekaProcess().start()

    uvloop.install()
    app = OrderServiceApplication()
    server = httpserver.HTTPServer(app)
    print(f'Started on http://127.0.0.1:{PORT}/')
    server.bind(PORT)
    try:
        ioloop = IOLoop.current()
        server.start(1)  # forks one process per cpu
        ex = ThreadPoolExecutor()
        ioloop.set_default_executor(ex)
        ioloop.start()
    except KeyboardInterrupt:
        sys.exit(0)


if __name__ == '__main__':
    main()
