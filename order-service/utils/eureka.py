from multiprocessing import Process
from time import sleep

from tornado.ioloop import IOLoop

from server.config import eureka


class EurekaProcess(Process):
    def __init__(self, **kwargs):
        super().__init__(daemon=True, **kwargs)

    def run(self) -> None:
        while True:
            try:
                print('Connecting to eureka')
                IOLoop.current().run_sync(eureka.register)
                sleep(40)
            except Exception as e:
                print('Error connecting to eureka', e)
                sleep(5)
