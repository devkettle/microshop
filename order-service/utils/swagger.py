from copy import deepcopy
from typing import Type

from tornado import web

from utils.custom_copy import function_deep_copy


def __gen_methods_by_handler(cls: Type[web.RequestHandler]):
    for method_name in web.RequestHandler.SUPPORTED_METHODS:
        method_name = method_name.lower()
        yield method_name, getattr(cls, method_name)


def class_spec_copy(cls: Type[web.RequestHandler]):
    class_name = cls.__name__
    cls = type(class_name, (cls,), {})
    handler_methods_copy(cls)

    return cls


def handler_methods_copy(cls: Type[web.RequestHandler]):
    for method_name, method in __gen_methods_by_handler(cls):
        new_method = function_deep_copy(method)
        if hasattr(method, 'path_spec'):
            new_method.path_spec = deepcopy(method.path_spec)

        setattr(cls, method_name, new_method)
