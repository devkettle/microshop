from urllib.parse import urljoin

from aiohttp import ClientSession

from server.config import eureka

_eureka_headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
}


async def get(service_name, url, session=None, **kwargs):
    return await request(service_name, 'get', url, session=session, **kwargs)


async def post(service_name, url, session=None, **kwargs):
    return await request(service_name, 'post', url, session=session, **kwargs)


async def request(service_name, method, url, session=None, **kwargs):
    session = session or ClientSession()
    eureka._session = ClientSession(headers=_eureka_headers)
    service = await eureka.get_app(service_name)
    instance = service['application']['instance'][0]
    url = urljoin(f"http://{instance['hostName']}:{instance['port']['$']}", url)
    async with session.request(method, url, **kwargs) as resp:
        return await resp.json()
